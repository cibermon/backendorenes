﻿using ChallengeOrenes.Datos.Manejadores;
using ChallengeOrenes.Dominio.Factorias;
using System;
using System.Collections.Generic;

namespace ChallengeOrenes.Datos.Daos
{
    internal abstract  class AbstractDataFactory<T,ID> : IFactoryDB<T,ID>
    {


        //Podriamos completar esta dao generica incluyendo en la fachada posibilidad de hacer consultas Linq.

        protected IManejadorBd manejador;

        /// <summary>
        /// interfaz que representa una entidad del sistema.
        /// </summary>
        protected  Type modelType { get; set; }

        /// <summary>
        /// tipo de clase que implementa el modelType.
        /// </summary>
        protected Type persistedType { get; set; }

        public AbstractDataFactory(IManejadorBd manejador)
        {
            this.manejador = manejador;
        }
        public T CreateInstance()
        {
            //Usamos reflexión para construir una instancia
            T instance = (T)Activator.CreateInstance(persistedType, false);
            return instance;
        }
        
        public bool Delete(T entity)
        {
            // TODO usar cualquier framework de persistencia, apoyandonos en un manejador
            throw new NotImplementedException();
        }

        public bool Delete(ID IdEntity)
        {
            // TODO usar cualquier framework de persistencia, apoyandonos en un manejador
            throw new NotImplementedException();
        }


        public T Save(T entity)
        {
            // TODO usar cualquier framework de persistencia, apoyandonos en un manejador
            throw new NotImplementedException();
        }

        public T SaveOrUpdate(T entity)
        {
            // TODO usar cualquier framework de persistencia, apoyandonos en un manejador
            throw new NotImplementedException();
        }

        public IList<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public T Get(ID id)
        {
            throw new NotImplementedException();
        }
    }

    
}
