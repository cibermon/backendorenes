﻿using ChallengeOrenes.Datos.Manejadores;
using ChallengeOrenes.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Datos.Daos
{
    internal class UsuariosDataFactory : AbstractDataFactory<Usuario,int>,IUsuarios
    {
        public UsuariosDataFactory(IManejadorBd manejador) : base(manejador)
        {
            this.persistedType = typeof(UsuarioImp);
            this.modelType  = typeof(Usuario);
        }
    }
}
