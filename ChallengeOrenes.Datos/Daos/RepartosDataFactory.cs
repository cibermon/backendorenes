﻿using ChallengeOrenes.Datos.Manejadores;
using ChallengeOrenes.Dominio;
using ChallengeOrenes.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Datos.Daos
{
    internal class RepartosDataFactory : AbstractDataFactory<Reparto,int>, IRepartos
    {

        public RepartosDataFactory(IManejadorBd manejador) : base(manejador)
        {
            this.persistedType = typeof(RepartoImp);
            this.modelType = typeof(Reparto);
        }

        public IList<Reparto> Get(QueryReparto query)
        {
            if (query?.Cliente != null)
            {
                //TODO aqui se puede usar consultas tipo Linq, algo parecido a esto: 
                //return Session.Linq<RepartoImp>.Where(u => u.Pedidos.any(z => z.Cliente = query.Cliente)).Cast<Reparto>().ToList;
            }
            else
            {
                if (query?.Pedido != null)
                {
                    //TODO aqui se puede usar consultas tipo Linq, algo parecido a esto: 
                    //return Session.Linq<RepartoImp>.Where(u => u.Pedidos.any(z => z.Id = query.Pedido.Id)).Cast<Reparto>().ToList;
                }
            }
            throw new NotImplementedException();
        }
    }
}
