﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Datos.Manejadores
{
    /// <summary>
    /// La clase que implemente este interfaz tiene que gestionar la session de la BD (apertura, reconexión, cierre, checkstatus)
    /// </summary>
    public interface IManejadorBd
    {
       
    }
}
