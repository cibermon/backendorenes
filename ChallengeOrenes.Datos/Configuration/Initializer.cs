﻿using ChallengeOrenes.Datos.ImplementacionClasesDominio;
using ChallengeOrenes.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Datos.Configuration
{
    public static class Initializer
    {

        public static bool CargarConfiguracion()
        {
            bool result = false;

            try
            {
                //TODO aqui es donde deberiamos construir la UnitOfWork 
                //para generar correctamente una factoria de datos en donde la session esté controlada, entre otras cosas.

                IDataService factoriaDatos = new DataService();

                ConfiguracionCatalogoServicios conf = new ConfiguracionCatalogoImp { dataFactory = factoriaDatos };

                CatalogoServicios.SetConfiguration(conf);
                result = true;
            }
            catch(Exception ex)
            {
                //TODO logeo de excepción
                throw new Exception ("No se ha podido cargar la configuración del sistema" ,ex);
            }

            return result;
        }

    }
}
