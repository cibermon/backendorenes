﻿using ChallengeOrenes.Dominio;

namespace ChallengeOrenes.Datos.Configuration
{
    internal class ConfiguracionCatalogoImp : ConfiguracionCatalogoServicios
    {
        public IDataService dataFactory { get; set; }
    }
}
