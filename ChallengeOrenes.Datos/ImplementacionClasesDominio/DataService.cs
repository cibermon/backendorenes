﻿using ChallengeOrenes.Datos.Daos;
using ChallengeOrenes.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Datos.ImplementacionClasesDominio
{
    internal class DataService : IDataService
    {

        
        private IUsuarios _usuarios = null;
        public IUsuarios Usuarios
        {
            get
            {
                if (_usuarios == null)
                {
                    _usuarios = new UsuariosDataFactory(null);
                }

                return _usuarios;
            }

        }

        public IPedidos Pedidos => throw new NotImplementedException();

        public IVehiculos Vehiculos => throw new NotImplementedException();


        private IRepartos _repartos = null;
        public IRepartos Repartos
        {
            get
            {
                if (_repartos == null)
                {
                    _repartos = new RepartosDataFactory(null);
                }

                return _repartos;
            }

        }
    }
}
