﻿using ChallengeOrenes.Controllers;
using ChallengeOrenes.Datos.Configuration;
using ChallengeOrenes.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Tests.Controllers
{
    [TestClass]
    public class RepartosControllerTest
    {
        [TestInitialize ]
        public void Initialize()
        {
            Assert.IsTrue(Initializer.CargarConfiguracion(), "Error de Carga de sistema");
        }

        [TestMethod]
        public void Index()
        {
            // Arrange
            RepartosController controller = new RepartosController();

            // Act
            IEnumerable<RepartoViewModel> result = controller.Get() as IEnumerable<RepartoViewModel >;

            // Assert
            Assert.IsNotNull(result);
        }


    }
}
