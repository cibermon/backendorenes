﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public class EnumUtil
    {

        public static string GetDefaultValue(Enum value)
        {
            if (value == null) return "";
            Type typ = value.GetType();
            FieldInfo fieldInfo = typ.GetField(value.ToString());
            if (fieldInfo == null) return "";
            DefaultValueAttribute[] attribs = (DefaultValueAttribute[])fieldInfo.GetCustomAttributes(typeof(DefaultValueAttribute), false);
            if (attribs.Length > 0)
            {
                return attribs[0].Value.ToString();
            }
            else
            {
                return "";
            }
        }

    }
}
