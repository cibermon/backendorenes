﻿using ChallengeOrenes.Dominio.Factorias;

namespace ChallengeOrenes.Dominio
{
    public interface IUsuarios :
        IFactoryDB <Usuario,int>
    {
    }
}