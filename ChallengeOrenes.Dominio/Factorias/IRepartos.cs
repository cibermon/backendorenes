﻿using ChallengeOrenes.Dominio.Entidades;
using ChallengeOrenes.Dominio.Factorias;
using System.Collections.Generic;

namespace ChallengeOrenes.Dominio
{
    public interface IRepartos : IFactoryDB<Reparto,int>
    {

        /// <summary>
        /// Obtiene los repartos, dependiendo de que queramos solicitar a través de la Query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IList<Reparto> Get(QueryReparto query);
    }

    public class QueryReparto
    {
        public Vehiculo Vehiculo { get; set; }

        public Pedido Pedido { get; set; }

        public Cliente Cliente { get; set; }
    }
}