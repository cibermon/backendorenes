﻿using ChallengeOrenes.Dominio.Factorias;

namespace ChallengeOrenes.Dominio
{
    public interface IVehiculos:IFactoryDB<Vehiculo,int>
    {
    }
}