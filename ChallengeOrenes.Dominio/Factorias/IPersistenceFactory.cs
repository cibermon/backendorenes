﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Dominio.Factorias
{
    public interface IPersistenceFactory<T,ID>
    {

        /// <summary>
        /// Persiste la entidad, si no existiese previamente
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        T Save(T entity);


        /// <summary>
        /// Persiste o actualiza la entidad
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        T SaveOrUpdate(T entity);


        /// <summary>
        /// Elimina la entidad del sistema de persistencia
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Delete(T entity);

        /// <summary>
        /// Elimina la entidad del sistema de persistencia, buscandola por id
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Delete(ID id);
    }
}
