﻿using System.Collections.Generic;

namespace ChallengeOrenes.Dominio.Factorias
{
    public interface IFactoryDB<T,ID>:ICreationFactory<T>,IPersistenceFactory <T,ID> 
    {

        IList<T> GetAll();

        T Get(ID id);

    }
}
