﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Dominio.Factorias
{
    public interface ICreationFactory<T>
    {

        /// <summary>
        /// Gestiona la creación de instancias de tipo T
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        T CreateInstance();


    }
}
