﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Dominio
{
    /// <summary>
    /// Representa el servicio de acceso y gestión de nuestras entidades del sistema. 
    /// </summary>
    public interface IDataService
    {

        IPedidos Pedidos { get; }

        IUsuarios Usuarios { get; }

        IVehiculos Vehiculos { get; }

        IRepartos Repartos { get; }

    }
}
