﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Dominio.Entidades
{
    public interface Pedido
    {
        string Id { get; set; }
        
        Cliente Cliente { get; set; }

        IList<LineaPedido> Lineas {get;set;}

    }

    public interface LineaPedido
    {
        //TODO Articulo, ...
    }
}
