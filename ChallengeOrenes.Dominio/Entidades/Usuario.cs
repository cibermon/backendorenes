﻿namespace ChallengeOrenes.Dominio
{
    public class Usuario
    {
        public string Id { get; set; }

        public string Alias { get; set; }

        public Cliente DatosPersonales { get; set; }
    }
}