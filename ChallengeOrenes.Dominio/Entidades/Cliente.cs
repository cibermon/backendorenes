﻿namespace ChallengeOrenes.Dominio
{
    public interface Cliente
    {
        string Id { get; set; }

        //TODO Identificación NIF/CIF/NIE/...
        string NombreCompleto { get; set; }

        Direccion DireccionPrincipal { get; set; }

    }

    public interface Direccion
    {
        string NombreVia { get; set; }

        string CodigoPostal { get; set; }

        Municipio Municipio { get; set; }
    }

    public interface Municipio
    {
        string Nombre { get; set; }
        Provincia Provincia { get; set; }

    }

    public class Provincia
    {
        string Nombre { get; set; }

        //TODO y se puede seguir con Pais
    }
}