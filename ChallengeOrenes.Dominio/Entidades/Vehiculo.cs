﻿namespace ChallengeOrenes.Dominio
{
    public interface Vehiculo
    {
        string id { get; set; }
        string Matricula { get; set; }
    }
}