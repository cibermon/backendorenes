﻿namespace ChallengeOrenes.Dominio.Entidades
{
    /// <summary>
    /// Entidad que representa una ubicación
    /// </summary>
    public interface Ubicacion
    {
        /// Desconozco si realmente existe un tipo de ubicación que no cumpla esta fachada. 
        
        string Latitud { get; set; }

        string Longitud { get; set; }
    }
}