﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Dominio.Entidades
{
    public interface Reparto
    {

        DateTime StartDate { get; set; }

        DateTime? LimitDate { get; set; }

        Vehiculo Vehiculo { get; set; }

        //TODO Se podria gestionar por choferes, si fuese necesario.

        IList<Pedido> Pedidos { get; set; }

        /// <summary>
        /// Representa las diferentes ubicaciones del reparto, siendo la ultima la más actual.
        /// </summary>
        IList<Ubicacion> Ubicaciones { get; set; }

    }
}
