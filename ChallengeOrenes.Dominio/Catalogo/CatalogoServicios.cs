﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Dominio
{
public sealed class CatalogoServicios
    {
        public static void SetConfiguration(ConfiguracionCatalogoServicios config)
        {
            _entidades = config.dataFactory;
        }

        private static IDataService _entidades;
        public static IDataService Entidades
        {
            get
            {
                if (_entidades == null)
                {
                    throw new Exception("Sistema no preparado");
                }
                return _entidades;
            }
        }

    }
}
