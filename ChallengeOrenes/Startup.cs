﻿using ChallengeOrenes.Datos.Configuration;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ChallengeOrenes.Startup))]
namespace ChallengeOrenes
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Initializer.CargarConfiguracion();
        }
    }
}
