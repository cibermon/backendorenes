﻿using Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ChallengeOrenes.Controllers
{
    public class NotificationController : Controller
    {
        [HttpPost]
        public async Task<ActionResult> Notify(TiposNotificaciones action, object data)
        {
            if (action != TiposNotificaciones.Nodef)
            {
                // Create an event with action 'action' and additional data
                await this.NotifyAsync(EnumUtil.GetDefaultValue(action), data);
            }
            

            return new EmptyResult();
        }

        public enum TiposNotificaciones
        {
            [Description("")]
            [DefaultValue("")]
            Nodef,
            [Description("Ubicacion Modificada")]
            [DefaultValue ("UbicacionChange")]
            UbicacionChange
        }

    }
}