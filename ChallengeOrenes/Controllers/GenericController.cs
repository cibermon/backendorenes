﻿using ChallengeOrenes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ChallengeOrenes.Controllers
{
 
    /// <summary>
    /// Controller encargado de, por un lado, mapear toda entidad que salga del sistema, y por otro lado, recuperar
    /// del sistema de cacheo la entidad original que nosotros enviamos, hidratando las diferencias sobre la propiedad entidad,
    /// la cual llevará asociada una entidad del sistema.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Query"></typeparam>
    public abstract class GenericEntityController<T, Query> : ApiController where T : IModel
    {

        public GenericEntityController()
        {

        }


        protected virtual T InitInstance(T m, Query q)
        {
            return m;
        }

        protected virtual T GetInstance(Query q)
        {
            T internalinstance = Activator .CreateInstance <T>();
            if (internalinstance != null)
            {
                internalinstance = InitInstance(internalinstance, q);
            }
            
            return internalinstance;
        }

        
        public IHttpActionResult Get(string queryStr = null)
        {
            string response = null;
            try
            {
                Query q = DeserializarQuery(queryStr);

                if (ValidateUser(q,null, this.Request))
                {
                    T internalinstance = GetInstance(q);

                    if (internalinstance != null)
                    {
                        MapController.GetInstance.Map(internalinstance);

                        response = ControllerUtil.GetInstance.Serializar(internalinstance);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else return Unauthorized();

            }
            catch (Exception ex)
            {
                return InternalServerError();
            }

            return Ok(response);
        }

     
        public IHttpActionResult Put(PutViewModel pam)
        {
            string response = null;

            try
            {
                T model = DeserializarModel(pam);
                if (model == null)
                {
                    return BadRequest();
                }
                T mappedentity = (T)MapController.GetInstance.GetMapped(model);
                if (mappedentity != null)
                {
                    if (ValidateUser(mappedentity, this.Request ))
                    {
                        MapController.GetInstance.Hidratar<T>(model, mappedentity);
                        this.DoPut(model);
                        response = ControllerUtil.GetInstance.Serializar(mappedentity);
                    }
                    else return Unauthorized();
                }
                else
                {
                    return BadRequest();
                }


            }
            catch (Exception ex)
            {
                return InternalServerError();
            }

            return Ok(response);
        }


        [HttpPost]
        public IHttpActionResult Post(T model)
        {
            try
            {
                if (model != null)
                {
                    if (ValidateUser(model, this.Request))
                    {
                        this.DoPost(model);
                    }
                    else
                    {
                        return Unauthorized();
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
            return Ok();
        }

      

        protected virtual Query DeserializarQuery(string queryStr)
        {
            Query q;
            if (!string.IsNullOrEmpty(queryStr))
            {
                q = ControllerUtil.GetInstance.DeserializarQuery<Query>(queryStr, true);
            }
            else
            {
                q = CreateDefaultQuery();
            }

            return q;
        }

        protected virtual Query CreateDefaultQuery()
        {
            return default(Query);
        }

        /// <summary>
        /// Metodo post que debe sobreescribir los hijos si quisieran tratar este metodo de comunicacion
        /// </summary>
        /// <param name="model"></param>
        protected virtual bool DoPost(T model)
        {
            //No hacemos nada, cada hijo debe tratar su post como vea conveniente
            return true;
        }

        /// <summary>
        /// Metodo post que debe sobreescribir los hijos si quisieran tratar este metodo de comunicacion
        /// </summary>
        /// <param name="model"></param>
        protected virtual bool DoPut(T model)
        {
            //No hacemos nada, cada hijo debe tratar su post como vea conveniente
            return true;
        }


        protected virtual T DeserializarModel(PutViewModel pam)
        {
            T result = default(T);
            if (pam != null)
            {
                if (!string.IsNullOrEmpty(pam.modelstr))
                {
                    result = ControllerUtil.GetInstance.Deserializar<T>(pam.modelstr);
                }
            }
            return result;
        }

        protected bool ValidateUser(T model, HttpRequestMessage request)
        {
            return ValidateUser(default(Query), model, request);
        }
        protected virtual bool ValidateUser(Query q, IModel currentModel, HttpRequestMessage context)
        {
            //TODO validar el usuario con respecto a los datos que solicita
            return true;
        }



    }
}