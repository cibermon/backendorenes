﻿using Newtonsoft.Json;
using System;

namespace ChallengeOrenes.Controllers
{
    internal class ControllerUtil
    {

        public static ControllerUtil GetInstance { get; set; } = new ControllerUtil();


        public string Serializar(object entidad)
        {
            string json = JsonConvert.SerializeObject(entidad,
                new JsonSerializerSettings
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                    ContractResolver = new SerializeContractResolver()
                });

            return json;
        }


        public T Deserializar<T>(string json)
        {
            T result = default(T);
            if (!string.IsNullOrEmpty(json))
            {
                result = JsonConvert.DeserializeObject<T>(json,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                        ContractResolver = new SerializeContractResolver()
                    });
            }
            return result;
        }



        public T DeserializarQuery<T>(string queryStr)
        {
            return DeserializarQuery<T>(queryStr, false);
        }

        public T DeserializarQuery<T>(string queryStr, bool base64Encoded)
        {
            T result = default(T);
            if (!string.IsNullOrEmpty(queryStr))
            {
                try
                {
                    string serializacion = queryStr;
                    if (base64Encoded)
                    {
                        byte[] base64EncodedBytes = Convert.FromBase64String(queryStr);
                        serializacion = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                    }

                    if (!string.IsNullOrEmpty(serializacion))
                        result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(serializacion, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore, DateFormatString = "yyyyMMdd" });
                }
                catch (Exception ex)
                {
                }
            }
            return result;
        }

    }
}