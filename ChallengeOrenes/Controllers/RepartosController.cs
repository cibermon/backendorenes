﻿using ChallengeOrenes.Dominio;
using ChallengeOrenes.Dominio.Entidades;
using ChallengeOrenes.Filters;
using ChallengeOrenes.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ChallengeOrenes.Controllers
{
    public class RepartosController : GenericEntityController<RepartoViewModel, QueryRepartosController>
    {
        protected override RepartoViewModel InitInstance(RepartoViewModel reparto, QueryRepartosController q)
        {
            if (reparto != null)
            {

                if (q != null)
                {
                    Usuario user = CatalogoServicios.Entidades.Usuarios.Get(q.idUsuario);
                    if (user != null)
                    {
                        reparto._reparto = CatalogoServicios.Entidades.Repartos.Get(new QueryReparto { Cliente = user.DatosPersonales })?.FirstOrDefault();
                    }
                }

            }
            return reparto;
        }

        protected virtual bool DoPost(RepartoViewModel model)
        {
            if (model._reparto != null)
                //TODO Si el reparto no existe o es mock, hidratar una entidad nueva y asociarla al viewmodel, además de persistirla
                CatalogoServicios.Entidades.Repartos.Save(model._reparto);
            return true;
        }

        protected virtual bool DoPut(RepartoViewModel model)
        {
            if (model._reparto != null)
                CatalogoServicios.Entidades.Repartos.SaveOrUpdate(model._reparto);

            return true;
        }
    }

    public class QueryRepartosController
    {
        public int idUsuario { get; set; }
    }

}
