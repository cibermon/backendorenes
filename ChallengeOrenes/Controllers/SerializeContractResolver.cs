﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.ComponentModel;
using System.Reflection;

namespace ChallengeOrenes.Controllers
{
    public class SerializeContractResolver : DefaultContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = null;
            if (member.GetCustomAttribute<BindableAttribute>(false) != null)
            {
                property = base.CreateProperty(member, memberSerialization);
            }

            return property;
        }
    }
}