﻿using ChallengeOrenes.Models;
using System;

namespace ChallengeOrenes.Controllers
{

    /// <summary>
    /// TODO Clase que se va a encargar de la gestión un sistema de cacheo para que nuestra aplicación mantenga la entidades que ha enviado,
    /// con el objetivo de tener unicidad de entidad y poder controlar que cambios sufren (y notificar si fuese necesario)
    /// Usaremos AutoMapped y para identificar, de manera univoca a la entidad, usamos la propiedad MappedId de los viewmodels
    /// </summary>
    internal class MapController
    {

        public static MapController GetInstance { get; set; } = new MapController();

        internal T GetMapped<T>(T model) where T : IModel
        {
            throw new NotImplementedException();
        }

        internal void Hidratar<T>(T model, T mappedentity)
        {
            throw new NotImplementedException();
        }

        internal void Map<T>(T internalinstance) where T : IModel
        {
            throw new NotImplementedException();
        }
    }
}