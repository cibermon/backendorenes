﻿using ChallengeOrenes.Dominio;
using ChallengeOrenes.Dominio.Entidades;
using ChallengeOrenes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChallengeOrenes.Controllers
{
    public class PedidosController : GenericEntityController<PedidoViewModel ,int>
    {

        protected override PedidoViewModel InitInstance(PedidoViewModel pedido, int q)
        {
            if (pedido != null)
            {

                if (q != 0)
                {
                    pedido._pedido = CatalogoServicios.Entidades.Pedidos.Get(q);
                }

            }
            return pedido;
        }

        protected virtual bool DoPost(PedidoViewModel model)
        {
            if (model._pedido != null)
                //TODO Si el pedido no existe o es mock, hidratar una entidad nueva y asociarla al viewmodel, además de persistirla
                CatalogoServicios.Entidades.Pedidos.Save(model._pedido);
            return true;
        }

        protected virtual bool DoPut(PedidoViewModel model)
        {
            if (model._pedido != null)
                CatalogoServicios.Entidades.Pedidos.SaveOrUpdate(model._pedido);

            return true;
        }
    }
}
