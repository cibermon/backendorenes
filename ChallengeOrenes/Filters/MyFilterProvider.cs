﻿using Helpers;
using Microsoft.AspNet.WebHooks;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using static ChallengeOrenes.Controllers.NotificationController;

namespace ChallengeOrenes.Filters
{
    public class MyFilterProvider : IWebHookFilterProvider
    {
        private readonly Collection<WebHookFilter> filters = new Collection<WebHookFilter>
    {
        new WebHookFilter { Name = EnumUtil.GetDefaultValue(TiposNotificaciones.UbicacionChange ) , Description = "Evento de notificación de ubicación alterada"},
    };

        public Task<Collection<WebHookFilter>> GetFiltersAsync()
        {
            return Task.FromResult(this.filters);
        }
    }
}