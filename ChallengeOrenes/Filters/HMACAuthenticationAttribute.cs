﻿using System.Linq;
using System.Web;

namespace ChallengeOrenes.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Runtime.Caching;
    using System.Security.Cryptography;
    using System.Security.Principal;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Filters;
    using System.Web.Http.Results;

    public class HMACAuthenticationAttribute : Attribute, IAuthenticationFilter
    {
        private static Dictionary<string, string> allowedApps = new Dictionary<string, string>();
        private readonly ulong requestMaxAgeInSeconds = 300;  // 5 mins

        public HMACAuthenticationAttribute()
        {
            if (allowedApps.Count == 0)
                allowedApps.Add("4d53bce03ec34c0a911182d4c228ee6c", "A93reRTUJHsCuQSHR+L3GxqOJyDmQpCgps102ciuabc=");
        }

        public bool AllowMultiple
        {
            get
            {
                return false;
            }
        }


        private async Task<bool> isValidRequest(HttpRequestMessage req, string APPId, string incomingBase64Signature, string nonce, string requestTimeStamp)
        {
            string requestContentBase64String = "";
            string requestUri = HttpUtility.UrlEncode(req.RequestUri.AbsoluteUri.ToLower());
            string requestHttpMethod = req.Method.Method;

            if (allowedApps.ContainsKey(APPId))
            {
                string sharedKey = allowedApps[APPId];


                if (isReplayRequest(nonce, requestTimeStamp))
                    return false;

                byte[] hash = await ComputeHash(req.Content);

                if (hash != null)
                    requestContentBase64String = Convert.ToBase64String(hash);

                // QUITAMOS LA URI DEL CONTENIDO FIRMADO PARA HABILITAR EXISTENCIA DE PROXY INVERSOS
                // Dim data As String = String.Format("{0}{1}{2}{3}{4}{5}", APPId, requestHttpMethod, requestUri, requestTimeStamp, nonce, requestContentBase64String)
                string data = string.Format("{0}{1}{2}{3}{4}", APPId, requestHttpMethod, requestTimeStamp, nonce, requestContentBase64String);


                byte[] secretKeyBytes = Convert.FromBase64String(sharedKey);

                byte[] signature = Encoding.UTF8.GetBytes(data);

                using (HMACSHA256 hmac = new HMACSHA256(secretKeyBytes))
                {
                    byte[] signatureBytes = hmac.ComputeHash(signature);
                    return (incomingBase64Signature.Equals(Convert.ToBase64String(signatureBytes), StringComparison.Ordinal));
                }
            }

            return false;
        }


        private bool isReplayRequest(string nonce, string requestTimeStamp)
        {
            if ((MemoryCache.Default.Contains(nonce)))
                return true;
            ulong serverTotalSeconds;
            ulong requestTotalSeconds;

            DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan currentTs = DateTime.UtcNow - epochStart;

            serverTotalSeconds = Convert.ToUInt64(currentTs.TotalSeconds);
            requestTotalSeconds = Convert.ToUInt64(requestTimeStamp);

            if (serverTotalSeconds > requestTotalSeconds)
            {
                if ((serverTotalSeconds - requestTotalSeconds) > requestMaxAgeInSeconds)
                    return true;
            }
            else if ((requestTotalSeconds - serverTotalSeconds) > requestMaxAgeInSeconds)
                return true;

            MemoryCache.Default.Add(nonce, requestTimeStamp, DateTimeOffset.UtcNow.AddSeconds(requestMaxAgeInSeconds));

            return false;
        }

        private static async Task<byte[]> ComputeHash(HttpContent httpContent)
        {
            byte[] hash = null;

            if (httpContent != null)
            {
                using (MD5 md5 = MD5.Create())
                {
                    byte[] Content = await httpContent.ReadAsByteArrayAsync();
                    if (Content.Length != 0)
                        hash = md5.ComputeHash(Content);
                }
            }

            return hash;
        }


        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            HttpRequestMessage req = context.Request;

            bool valido = false;

            if (req.Headers.Authorization != null)
            {
                string rawAuthzHeader = req.Headers.Authorization.Parameter;
                string[] autherizationHeaderArray = GetAutherizationHeaderValues(rawAuthzHeader);


                if (autherizationHeaderArray != null)
                {
                    string APPId = autherizationHeaderArray[0];
                    string incomingBase64Signature = autherizationHeaderArray[1];
                    string nonce = autherizationHeaderArray[2];
                    string requestTimeStamp = autherizationHeaderArray[3];

                    Task<bool> isValid = isValidRequest(req, APPId, incomingBase64Signature, nonce, requestTimeStamp);

                    if (isValid.Result)
                    {
                        GenericPrincipal currentPrincipal = new GenericPrincipal(new GenericIdentity(APPId), null/* TODO Change to default(_) if this is not a reference type */);
                        context.Principal = currentPrincipal;
                        valido = true;
                    }
                }
            }

            if (!valido)
                context.ErrorResult = new UnauthorizedResult(new List<AuthenticationHeaderValue>(), context.Request);

            return Task.FromResult(0);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            context.Result = new ResultWithChallenge(context.Result);
            return Task.FromResult(0);
        }

        private string[] GetAutherizationHeaderValues(string rawAuthzHeader)
        {
            string[] credArray = rawAuthzHeader.Split(':');

            if (credArray.Length == 4)
                return credArray;
            else
                return null;
        }

        public class ResultWithChallenge : IHttpActionResult
        {
            private readonly string authenticationScheme = "Orenes";
            private readonly IHttpActionResult actionResult;

            public ResultWithChallenge(IHttpActionResult actionResult)
            {
                this.actionResult = actionResult;
            }

            public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                HttpResponseMessage response = await actionResult.ExecuteAsync(cancellationToken);

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                    response.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue(authenticationScheme));

                return response;
            }
        }
    }

}