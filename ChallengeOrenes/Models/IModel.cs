﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeOrenes.Models
{
    public interface IModel
    {

        /// <summary>
        /// ID de mapeo
        /// </summary>
        int _mappedId { get; set; }
    }
}
