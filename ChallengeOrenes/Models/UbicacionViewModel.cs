﻿using ChallengeOrenes.Dominio.Entidades;
using Newtonsoft.Json;

namespace ChallengeOrenes.Models
{
    public class UbicacionViewModel:AbstractEntityViewModel 
    {

        [JsonIgnore]
        internal Ubicacion _ubicacion;

        public UbicacionViewModel():base() { }

        internal UbicacionViewModel(Ubicacion entity):base()
        {
            this._ubicacion = entity;
        }

        //TODO implementar usando _ubicacion
    }
}