﻿using ChallengeOrenes.Dominio.Entidades;
using Newtonsoft.Json;

namespace ChallengeOrenes.Models
{
    public class PedidoViewModel : AbstractEntityViewModel
    {

        public PedidoViewModel() : base() { }

        [JsonIgnore]
        internal Pedido _pedido;

        internal PedidoViewModel(Pedido entity):base()
        {
            this._pedido = entity;
        }

        //TODO implementar usando _pedido
    }
}