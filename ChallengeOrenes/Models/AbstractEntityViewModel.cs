﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChallengeOrenes.Models
{
    public class AbstractEntityViewModel: IModel
    {

        public int _mappedId { get; set; }

        public AbstractEntityViewModel()
        {
            this._mappedId = GetHashCode();
        }
    }
}