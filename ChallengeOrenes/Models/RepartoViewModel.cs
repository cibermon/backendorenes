﻿using ChallengeOrenes.Dominio;
using ChallengeOrenes.Dominio.Entidades;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ChallengeOrenes.Models
{
    public class RepartoViewModel : AbstractEntityViewModel
    {
        [JsonIgnore]
        internal Reparto _reparto;

        public RepartoViewModel() : base() { }


        private DateTime? _startDate;


        [Bindable(true,BindingDirection.OneWay )]
        public DateTime StartDate
        {
            get
            {
                if (_reparto != null && _startDate == null)
                {
                    _startDate = _reparto.StartDate;
                }
                return _startDate.GetValueOrDefault();
            }
            set
            {
                _startDate = value;
                if (_reparto != null)
                {
                    _reparto.StartDate = value;
                }
            }
        }

        private DateTime? _limitDate;
        [Bindable(true, BindingDirection.OneWay)]
        public DateTime? LimitDate
        {
            get
            {
                if (_reparto?.LimitDate != null && _limitDate == null)
                {
                    _limitDate = _reparto.LimitDate.Value;
                }

                return _limitDate;
            }
            set
            {
                if (value != null)
                {
                    _limitDate = value.Value;
                    if (_reparto != null) _reparto.LimitDate = value.Value;
                }
                else
                {
                    _limitDate = null;
                    if (_reparto != null) _reparto.LimitDate = null;
                }

            }
        }

        private VehiculoViewModel _vehiculo;

        [Bindable(true, BindingDirection.OneWay)]
        public VehiculoViewModel Vehiculo
        {
            get
            {
                if (_reparto != null && _vehiculo == null)
                {
                    _vehiculo = new VehiculoViewModel(_reparto.Vehiculo);
                }

                return _vehiculo;
            }
            set
            {
                _vehiculo = value;
                if (_reparto != null)
                {
                    if (value != null)
                        _reparto.Vehiculo = value._vehiculo;
                    else
                        _reparto.Vehiculo = null;
                }
            }
        }

        [Bindable(true, BindingDirection.TwoWay)]
        private ObservableCollection<PedidoViewModel> _pedidos;
        public ObservableCollection<PedidoViewModel> Pedidos
        {
            get
            {
                if (_reparto != null && _pedidos == null)
                {
                    _pedidos = new ObservableCollection<PedidoViewModel>(_reparto.Pedidos.Select(u => new PedidoViewModel(u)));
                }

                return _pedidos;
            }
            set
            {
                _pedidos = value;
                if (_reparto != null)
                {
                    //TODO resolver adiciones y eliminaciones, puesto que esta asignación se hará al hidratar el viewmodel con la entidad asociada, 
                    // la cual enviamos previamente.
                    //if (_reparto != null) _reparto.Pedidos = value;
                }
            }
        }

        [Bindable(true, BindingDirection.TwoWay)]
        private ObservableCollection<UbicacionViewModel> _ubicaciones;
        public ObservableCollection<UbicacionViewModel> Ubicaciones
        {
            get
            {
                if (_reparto != null && _ubicaciones == null)
                {
                    _ubicaciones = new ObservableCollection<UbicacionViewModel>(_reparto.Ubicaciones.Select(u => new UbicacionViewModel(u)));
                }

                return _ubicaciones;
            }
            set
            {
                _ubicaciones = value;
                if (_reparto != null)
                {
                    //TODO resolver adiciones y eliminaciones, puesto que esta asignación se hará al hidratar el viewmodel con la entidad asociada, 
                    // la cual enviamos previamente.
                    //if (_reparto != null) _reparto.Ubicaciones = value;

                    //TOOD enviar una notificación a través de NotificationController. 

                }
            }
        }


    }
}