﻿using ChallengeOrenes.Dominio;
using Newtonsoft.Json;

namespace ChallengeOrenes.Models
{
    public class VehiculoViewModel : AbstractEntityViewModel
    {
        [JsonIgnore]
        internal Vehiculo _vehiculo;

        public VehiculoViewModel():base()
        {

        }

        internal VehiculoViewModel(Vehiculo vehiculo):base()
        {
            this._vehiculo = vehiculo;
        }

        //TODO implementar usando _vehiculo
    }
}